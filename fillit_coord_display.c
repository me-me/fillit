/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_coord_display.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/22 12:37:01 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/24 03:19:01 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	fillit_coord_display(char point[][3])
{
	int y;

	y = 0;
	while (point[y][1] != FILE_END)
	{
		ft_putchar(point[y][0]);
		ft_putstr(" (");
		ft_putnbr(point[y][1]);
		ft_putstr(", ");
		ft_putnbr(point[y][2]);
		ft_putstr(")\n");
		y++;
	}
}
