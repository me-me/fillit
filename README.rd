
Fillit is a project delivered by 42 school.

Introduction:

Fillit is a project allowing you to discover and / or familiarize yourself with
the recurring problem solving in programming: the search for an optimal solution
among a very large number of possibilities, within a reasonable time. 

In the case of this project, it will be necessary to arrange the tetriminos between them 
and determine the smallest square possible.

To compile the file:
	1. git clone <link>
	2. make

An executable will be created "fillit"

To execute the program you need an input file which include tertriminos in a specific format

Samlpe of a valid tetriminos file:

..#.
..#.
.##.
....

#...
##..
.#..
....

.##.
..##
....
....

....
..##
..##
....

An other example from a shell:==================================

$> cat sample.fillit | cat -e
...#$
...#$
...#$
...#$
$
....$
....$
....$
####$
$
.###$
...#$
....$
....$
$
....$
..##$
.##.$
....$
$
....$
.##.$
.##.$
....$
$
....$
....$
##..$
.##.$
$
##..$
.#..$
.#..$
....$
$
....$
###.$
.#..$
....$
$> ./fillit sample.fillit | cat -e
ABBBB.$
ACCCEE$
AFFCEE$
A.FFGG$
HHHDDG$
.HDD.G$

================================================

To execute the program:
./fillit sample.fillit

	