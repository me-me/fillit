/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_compute.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/24 01:16:37 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/26 13:16:45 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	test_square_limit(int coord[][3], char grid[][3], int xg, int side)
{
	int x;
	int i;
	int u;

	x = 0;
	i = coord[x][1];
	u = coord[x][1];
	while (coord[x][1] != FILE_END)
	{
		if (coord[x][1] < i)
			i = coord[x][1];
		if (coord[x][1] > u)
			u = coord[x][1];
		x++;
	}
	if (u - i > side - i || grid[xg][1] - i > grid[xg][1] - 1)
		return (-13);
	return (0);
}

static void	conv_coord(char grid[][3], char tetras[][3], int coord[][3], int xt)
{
	int y;
	int xg;
	int diffx;
	int diffy;

	xg = 0;
	y = grid[xg][2];
	while (grid[xg][2] == y)
		xg++;
	coord[5][2] = xg;
	y = 0;
	xg = coord[5][1];
	diffx = grid[xg][1] - tetras[xt][1];
	diffy = grid[xg][2] - tetras[xt][2];
	while (tetras[xt][1] != TETRA_END && tetras[xt][1] != FILE_END)
	{
		coord[y][0] = tetras[xt][0];
		coord[y][1] = tetras[xt][1] + diffx;
		coord[y][2] = tetras[xt][2] + diffy;
		xt++;
		y++;
	}
	coord[y][1] = FILE_END;
	coord[y][2] = FILE_END;
}

static int	tetra_place(char grid[][3], char tetras[][3], int xg, int xt)
{
	int side;
	int y;
	int coord[6][3];

	coord[5][1] = xg;
	conv_coord(grid, tetras, coord, xt);
	side = coord[5][2];
	if (test_square_limit(coord, grid, xg, side) < 0)
		return (-13);
	y = 0;
	while (coord[y][1] != TETRA_END && coord[y][1] != FILE_END)
	{
		if (side * (coord[y][2] - 1) + coord[y][1] - 1 > side * side - 1 \
			|| grid[side * (coord[y][2] - 1) + coord[y][1] - 1][0] != '.')
			return (-13);
		y++;
	}
	y = 0;
	while (coord[y][1] != TETRA_END && coord[y][1] != FILE_END)
	{
		grid[side * (coord[y][2] - 1) + coord[y][1] - 1][0] = coord[y][0];
		y++;
	}
	return (3);
}

static void	tetra_erase(char grid[][3], char c)
{
	int r;

	r = 0;
	while (grid[r][1] != FILE_END)
	{
		if (grid[r][0] == c)
			grid[r][0] = '.';
		r++;
	}
}

int			fillit_compute(char grid[][3], char tetras[][3], int xg, int xt)
{
	if (tetras[xt][1] == TETRA_END)
		xt++;
	if (tetras[xt][1] == FILE_END)
		return (7);
	while (grid[xg][0] != '.' && grid[xg][0] != FILE_END)
		xg++;
	if (grid[xg][1] == FILE_END)
		return (-15);
	if (tetra_place(grid, tetras, xg, xt) >= 0)
	{
		if (fillit_compute(grid, tetras, 0, xt + 4) >= 0)
			return (7);
		tetra_erase(grid, tetras[xt][0]);
	}
	if (fillit_compute(grid, tetras, xg + 1, xt) >= 0)
		return (7);
	return (-15);
}
