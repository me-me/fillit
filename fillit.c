/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 21:30:08 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/25 15:16:03 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <unistd.h>
#define T_COUNT buf[21]

static void	memini(char t[][3])
{
	int x;
	int a;
	int u;

	a = 'A';
	x = 0;
	while (a <= 'Z')
	{
		u = 0;
		while (u++ < 4)
		{
			t[x][0] = a;
			t[x][1] = INI;
			t[x++][2] = INI;
		}
		t[x][0] = TETRA_END;
		t[x][1] = INI;
		t[x++][2] = INI;
		a++;
	}
	t[--x][0] = FILE_END;
}

static int	handle_errors(int r)
{
	ft_putstr("error\n");
	return (r >= 0 ? r : -r);
}

int			main(int ac, char **ar)
{
	int		f;
	int		t;
	int		fd;
	char	buf[22];
	char	tetras[TETRAS_MAX * 5][3];

	if (ac != 2)
		return (handle_errors(-6));
	fd = INI;
	memini(tetras);
	T_COUNT = TETRAS_MAX;
	while ((f = fillit_file_read(ar[1], &fd, buf)) == 21 || f == 21 - 1)
	{
		if (!--T_COUNT)
			return (handle_errors(-9));
		if ((t = fillit_file_check(buf, tetras, f)) < 0)
			return (handle_errors(t));
	}
	close(fd);
	if (f < 0 || t != 21 - 1)
		return (handle_errors(2));
	if (fillit_tetras_check(tetras) >= 0)
		return (fillit_tetras_place(tetras));
	return (handle_errors(-8));
}
