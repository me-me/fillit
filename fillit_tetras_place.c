/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_tetras_place.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 11:54:06 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/26 13:30:41 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <stdlib.h>

	static char	(*grid_create(int side))[][3]
	{
	int		y;
	int		u;
	char	(*grid)[(side * side) + 1][3];

	y = 0;
	u = 0;
	grid = (char (*)[][3])malloc(sizeof(*grid));
	if (!grid)
		return (NULL);
	while (y != side * side)
	{
		(*grid)[y][0] = '.';
		(*grid)[y][1] = u % side + 1;
		(*grid)[y][2] = u / side + 1;
		y++;
		u++;
	}
	(*grid)[y][0] = FILE_END;
	(*grid)[y][1] = FILE_END;
	(*grid)[y][2] = FILE_END;
	return (grid);
}

int				fillit_tetras_place(char tetras[][3])
{
	int		i;
	int		side;
	char	(*grid)[][3];

	i = -1;
	side = 2;
	grid = NULL;
	while (i < 0)
	{
		if (grid)
			free(grid);
		if (!(grid = grid_create(side)))
			return (-100);
		i = fillit_compute(*grid, tetras, 0, 0);
		side++;
	}
	fillit_square_display(*grid, --side);
	free(grid);
	return (i);
}
