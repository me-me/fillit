# Fillit #
Fillit est un programme qui prendra en paramètre un ﬁchier décrivant une liste de Tetriminos qu’il devra ensuite agencer entre eux pour former le plus petit carré possible. Le but est
bien entendu de trouver ce plus petit carré le plus rapidement possible malgré un nombre d’agencements qui croît de manière explosive avec chaque pièce supplémentaire.