/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_square_display.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/21 20:38:12 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/21 22:35:40 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	fillit_square_display(char grid[][3], int side)
{
	int u;
	int i;
	int x;

	u = 0;
	i = 0;
	x = 0;
	while (u < side)
	{
		while (i < side)
		{
			ft_putchar(*grid[x]);
			x++;
			i++;
		}
		ft_putchar('\n');
		u++;
		i = 0;
	}
}
