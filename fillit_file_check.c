/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_file_check.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 22:46:55 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/22 13:45:50 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		fillit_coord_store(int x, int y, char tetras[][3], int *blocs)
{
	int n;
	int i;

	n = TETRAS_MAX * 5;
	i = 0;
	while (n && tetras[i][1] != INI)
	{
		n--;
		i++;
	}
	tetras[i][1] = ++x;
	tetras[i][2] = ++y;
	(*blocs)++;
}

static int		handle_return(char *file, int blocs, char tetras[][3], int f)
{
	int n;
	int x;

	if (f < 0)
		return (-2);
	if (blocs != 4)
		return (-5);
	n = TETRAS_MAX * 5;
	x = 0;
	while (n-- && tetras[x][1] != INI)
		x++;
	if (f == 20)
	{
		tetras[x][1] = FILE_END;
		tetras[x][2] = FILE_END;
		return (f);
	}
	if (*(file) == '\n')
	{
		tetras[x][1] = TETRA_END;
		tetras[x][2] = TETRA_END;
		return (3);
	}
	return (-4);
}

int				fillit_file_check(char *file, char tetras[][3], int t)
{
	int e;
	int f;
	int x;
	int	y;
	int	blocs;

	blocs = 0;
	y = 0;
	while (y < SIDE_LEN && (e = ft_strtillclen(file, '\n')) == SIDE_LEN + 1)
	{
		x = 0;
		while (x < SIDE_LEN)
		{
			if ((f = ft_isoneof("#.", (int)*(file++))) == '#')
				fillit_coord_store(x, y, tetras, &blocs);
			else if (f != '.')
				return (-3);
			x++;
		}
		file++;
		y++;
	}
	e != SIDE_LEN + 1 ? t = -2 : e;
	return (handle_return(file, blocs, tetras, t));
}
