/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 22:37:21 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/24 15:55:52 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# define TETRAS_MAX (26 + 1)
# define INI -33
# define TETRA_END -22
# define FILE_END -11
# define SIDE_LEN 4

int		fillit_file_read(char *ar, int *fd, char *buf);
int		fillit_file_check(char *file, char tetras[][3], int t);
int		fillit_tetras_check(char tetras[][3]);
int		fillit_tetras_place(char tetras[][3]);
void	fillit_square_display(char grid[][3], int side);
void	fillit_coord_display(char point[][3]);
int		fillit_compute(char grid[][3], char tetras[][3], int xg, int xt);
void	ft_putchar(char c);
void	ft_putnbr(int n);
int		ft_strtillclen(char const *s, int c);
int		ft_isoneof(char *chrs, int c);
void	ft_putstr(char const *s);

#endif
