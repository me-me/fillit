/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_tetras_check.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/19 14:57:41 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/22 14:23:25 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	bloc_check(char tetras[][3], int z, int xbloc0, int y)
{
	int o;
	int b;

	o = 0;
	while ((b = tetras[xbloc0][y]) != TETRA_END && b != FILE_END)
	{
		if (tetras[z][y] == b && tetras[z] != tetras[xbloc0])
		{
			if (y == 1)
				if ((b = tetras[z][2] - tetras[xbloc0][2]) == 1 || b == -1)
					o++;
			if (y == 2)
				if ((b = tetras[z][1] - tetras[xbloc0][1]) == 1 || b == -1)
					o++;
		}
		xbloc0++;
	}
	return (o);
}

static int	tetra_check(char tetras[][3], int z)
{
	int u;
	int xbloc0;

	xbloc0 = z;
	u = 0;
	while (tetras[z][1] != TETRA_END && tetras[z][1] != FILE_END)
	{
		u += bloc_check(tetras, z, xbloc0, 1);
		u += bloc_check(tetras, z, xbloc0, 2);
		z++;
	}
	if (u != 6 && u != 8)
		return (-8);
	return (0);
}

int			fillit_tetras_check(char tetras[][3])
{
	int z;

	z = 0;
	while (tetras[z][1] != FILE_END)
	{
		if (z)
			z++;
		if (tetra_check(tetras, z) < 0)
			return (-8);
		z += 4;
	}
	return (0);
}
