NAME  = fillit
PS = .
PI = .
PO = .
SRC  = $(patsubst %.c,$(PS)/%.c,\
	fillit.c	\
	fillit_file_read.c	\
	fillit_tetras_check.c	\
	fillit_tetras_place.c	\
	fillit_compute.c	\
	fillit_square_display.c \
	ft_putchar.c \
	ft_putnbr.c	\
	ft_strtillclen.c	\
	ft_isoneof.c \
	ft_putstr.c \
	fillit_file_check.c	\
	fillit_coord_display.c	\
)
OBJ   = $(SRC:$(PS)%.c=$(PO)%.o)

all: $(NAME)

$(NAME): $(OBJ) 
	gcc -Wall -Wextra -Werror -I$(PI) $(OBJ) -o fillit

$(OBJ):
	gcc -Wall -Wextra -Werror -c -I$(PI) $(SRC)

re: fclean all

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)
