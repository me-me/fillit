/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_file_read.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/18 11:07:54 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/24 03:22:11 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

int	fillit_file_read(char *ar, int *fd, char *buf)
{
	if (*fd == INI && (*fd = open(ar, O_RDONLY)) < 0)
		return (*fd);
	return (read(*fd, buf, 21));
}
