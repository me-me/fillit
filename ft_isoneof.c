/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isoneof.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: brnsabyu <brnsabyu@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/17 23:41:13 by brnsabyu          #+#    #+#             */
/*   Updated: 2017/01/20 00:53:20 by brnsabyu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_isoneof(char *chrs, int c)
{
	if (!chrs)
		return (-100);
	while (*chrs && c != *chrs)
	{
		chrs++;
	}
	return (c == *chrs ? c : -1);
}
